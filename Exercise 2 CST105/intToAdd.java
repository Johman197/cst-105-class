/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author calebedwards
 */
import java.util.Scanner;

public class intToAdd {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a, b, c, d, e;

        System.out.println("Enter 5 numbers for addition");
        a = input.nextInt();
        b = input.nextInt();
        c = input.nextInt();
        d = input.nextInt();
        e = input.nextInt();

        System.out.println("The sum of " + a + " + " + b + " + " + c + " + "
                + d + " + " + e + " is: " + (a + b + c + d + e));
    }
}
