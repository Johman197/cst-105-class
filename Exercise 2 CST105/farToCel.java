/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author calebedwards
 *
 */
import java.util.Scanner;

public class farToCel {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Enter a fahrenheit temperature: ");
        double enterFahr = input.nextInt();
        System.out.println(enterFahr + "degrees fahrenheit is " + (enterFahr - 32) * 5 / 9 + " degrees celcius");

        System.out.println("Enter a celcius temperature");
        double enterCel = input.nextInt();
        System.out.println(enterCel + "degrees celcius is " + (enterCel * 9 / 5) + 32);
    }

}
