package com.calebedwards;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    public static void main(String[] args)
    {
        char[][] arr = new char[20][45];
        String fileText = getFileText();

        //Read filetext into array
        int n = 0;
        for(int i = 0; i < 20; i++) //Row
        {
            for(int j = 0; j < 45; j++) //Column
            {
                //Checking if we are done read characters in filetext
                if(n < fileText.length()) //fileText.length() returns number of characters in file content.
                {
                    if (fileText.charAt(n) == ' ') {
                        arr[i][j] = '_';
                    } else {
                        arr[i][j] = fileText.charAt(n);
                    }

                    n++; //n = n + 1;
                }
                else
                {
                    arr[i][j] = '@';
                }
            }
        }

        //print array
        printArray(arr);

        String outputString = "";
        for(int j = 0; j < 45; j++) //Column
        {
            if(arr[0][j] == '@') break;

            for(int i = 0; i < 20; i++) //Row
            {
                if(arr[i][j] == '@') break;
                outputString += (arr[i][j] == '_') ? " " : arr[i][j];
            }
        }

        System.out.println("Final Output:");
        System.out.println(outputString);

    }

    private static void printArray(char[][] arr)
    {
        for(int i = 0; i < 20; i++)
        {
            for(int j = 0; j < 45; j++)
            {
                System.out.print(arr[i][j]);
            }
            System.out.print("\n");
        }
    }

    private static String getFileText()
    {
        String fileText = "";
        try
        {
            File file = new File("milk.txt");

            //Buffer Reader: Reads content of text file line by line
            BufferedReader br = new BufferedReader(new FileReader(file));
            String st;
            while ((st = br.readLine()) != null)
            {
                fileText += st;
            }
        }
        catch(IOException ex)
        {
            System.out.print(ex.getMessage());
        }

        return fileText;
    }
}
