package w3e3;

/**
 * @author calebedwards
 */
import java.util.Scanner;

public class W3E3 {

    public static void main(String[] args) {

        int number = (int) (Math.random() * 101);

        Scanner input = new Scanner(System.in);
        System.out.println("Guess a number between 1 and 10000");

        int guess = 0;
        int minimum = 1;
        int maximum = 10000;
        while (guess != number) {

            System.out.println("\nEnter your guess: ");

            guess = input.nextInt();
            if (guess < minimum || guess > maximum) {
                System.out.println("Please enter a number between 1 and 10,000");
            } else {
                if (guess > number) {
                    System.out.println("Your number is too high");
                }
                if (guess < number) {
                    System.out.println("Your guess is too low");
                }
                if (guess == number) {
                    System.out.println("Yes, the number is " + number);
                }
            }
        }

    }

}

//Thank you for your work. Your logic is okay but here are my main comments:
//- You needed to print the range when the user does not guess the number correct
//- You need to calculate and print the # of guesses when the user finally gets the right value.
//The above two pieces were missing in your logic and in your flowchart.
